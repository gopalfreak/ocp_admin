!!! The project is created by deployment.yaml !!!
oc apply -f deployment.yaml

!!! The secret is created by the annotation !!!
oc annotate service/server service.beta.openshift.io/serving-cert-secret-name=server-secret

# Create and annotate configmap
oc create cm ca-bundle
oc annotate configmap ca-bundle service.beta.openshift.io/inject-cabundle=true

# Create untrusting-client pod and test
oc apply -f untrusting-client.yaml
oc exec untrusting-client -- curl -s https://server.network-svccerts.svc

# Create trusting-client pod and test
oc apply -f trusting-client.yaml
oc exec trusting-client -- curl -s https://server.network-svccerts.svc
